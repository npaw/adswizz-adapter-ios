//
//  UIWindow+SafeArea.h
//  BasicAdRequestExampleObjC
//
//  Created by Teodor Cristea on 17/09/2019.
//  Copyright © 2020 Inc, AdsWizz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (SafeArea)

- (CGFloat)safeAreaTop;
- (CGFloat)safeAreaBottom;

@end
