//
//  MenuViewController.swift
//  AdswizzDemo
//
//  Created by Elisabet Massó on 29/6/21.
//

import UIKit
import YouboraLib
import YouboraConfigUtils

class MenuViewController: UIViewController {

    @IBOutlet weak var adTypeTf: UITextField!
    @IBOutlet weak var playBtn: UIButton!

    var plugin: YBPlugin?
    
    private var selectedApp: String?
    private var appList = ["BasicAdRequest", "BasicVideoAd"]

    override func viewDidLoad() {
        super.viewDidLoad()

        YBLog.setDebugLevel(.debug)
        
        addSettingsButton()
        
        setUpDropDown()
        closeDropDown()
        pickerView(adTypeTf.inputView as! UIPickerView, didSelectRow: 0, inComponent: 0)
        
    }
    
    public static func initFromXIB() -> MenuViewController? {
        return MenuViewController(nibName: String(describing: MenuViewController.self), bundle: Bundle(for: MenuViewController.self))
    }

}

// MARK: - Settings Section
extension MenuViewController {
    
    func addSettingsButton() {
        guard let navigationController = self.navigationController else { return }
        addSettingsToNavigation(navigationBar: navigationController.navigationBar)
    }
    
    func addSettingsToNavigation(navigationBar: UINavigationBar) {
        let settingsButton = UIBarButtonItem(title: "Settings", style: .done, target: self, action: #selector(navigateToSettings))
        navigationBar.topItem?.rightBarButtonItem = settingsButton
    }
    
}

// MARK: - Navigation Section
extension MenuViewController {
    
    @IBAction func sendOfflineEvents(_ sender: UIButton) {
        let options = YouboraConfigManager.getOptions()
        options.offline = false
        
        plugin = YBPlugin(options: options)
        
        for _ in 1...3 {
            plugin?.fireOfflineEvents()
        }
    }
    
    @IBAction func playButtonPressed(_ sender: UIButton) {
        if sender == playBtn {
            
            var playerViewController: UIViewController
            if selectedApp == "BasicAdRequest" {
                playerViewController = BasicAdRequestViewController()
            } else {
                playerViewController = ViewController()
            }
            
            navigateToViewController(viewController: playerViewController)
            return
        }
    }
    
    @objc func navigateToSettings() {
        guard let _ = navigationController else {
            navigateToViewController(viewController: YouboraConfigViewController.initFromXIB(animatedNavigation: false))
            return
        }
        
        navigateToViewController(viewController: YouboraConfigViewController.initFromXIB())
    }
    
    func navigateToViewController(viewController: UIViewController) {
        guard let navigationController = navigationController else {
            present(viewController, animated: true, completion: nil)
            return
        }
        
        navigationController.pushViewController(viewController, animated: true)
    }
    
}

// MARK: - App Drop-Down Section
extension MenuViewController {
    
    func setUpDropDown() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        adTypeTf.inputView = pickerView
    }
    
    func closeDropDown() {
        let button = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(dismissDropDown))
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.setItems([button], animated: true)
        toolbar.isUserInteractionEnabled = true
        
        adTypeTf.inputAccessoryView = toolbar
    }
    
    @objc func dismissDropDown() {
        self.view.endEditing(true)
    }
    
}

// MARK: - UIPickerViewDelegate, UIPickerViewDataSource
extension MenuViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return appList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return appList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedApp = appList[row]
        adTypeTf.text = selectedApp
    }
    
}

// MARK: - UITextFieldDelegate
extension MenuViewController: UITextFieldDelegate {
    
}
