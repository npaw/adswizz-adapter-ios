# YouboraAdswizzAdapter #

A framework that will collect several video events from the BitmovinPlayer and send it to the back end

# Installation #

#### Cloning the repo ####

In shell located in your project folder run

```bash
git clone https://bitbucket.org/npaw/adswizz-adapter-ios.git
```

Add the target in your Podfile

```bash
target 'YouboraAdswizzAdapter' do
	project 'adswizz-adapter-ios/YouboraAdswizzAdapter.xcodeproj'
	use_frameworks!
	# Pods for YouboraAdswizzAdapter
	pod 'YouboraLib', '~> 6.5'
end
```

Then run

```bash
pod install
```

And then

* Select the project file in the "Project" navigator column, and the target in the project and targets list
* In the "General" tab scroll down to the "Linked Frameworks and Libraries" section and click the "+" button
* Now look for the YouboraAdswizzAdapter.framework in the options, select it and press "Add"

## How to use ##

### Start plugin and options ###

```swift
// Import
import YouboraLib

...

// Config Options and init plugin (do it just once for each play session)

var options: YBOptions {
    let options = YBOptions()
    options.contentResource = "http://example.com"
    options.accountCode = "accountCode"
    options.adResource = "http://example.com"
    options.contentIsLive = NSNumber(value: false)
    return options;
}
    
lazy var plugin = YBPlugin(options: options)
```

For more information about the options you can check [here](http://developer.nicepeopleatwork.com/apidocs/ios6/Classes/YBOptions.html)

### YBAdswizzAdapter ###

```swift
import YouboraAdswizzAdapter
...

// Once you have your player and plugin initialized you can set the adapter
plugin.adsAdapter = YBAdswizzAdsAdapter(player: adManager)

...

// When the view is going to be destroyed you can force stop and clean the adapters in order to make sure you avoid retain cycles  
plugin.fireStop()
plugin.removeAdapter()
plugin.removeAdsAdapter()
```

## Run samples project ##

###### Via the cloned repo ######

Navigate to Samples folder and execute: 

```bash
pod install
```

The project should be ready now to work