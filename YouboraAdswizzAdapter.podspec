Pod::Spec.new do |spec|

  spec.name         = 'YouboraAdswizzAdapter'
  spec.version      = '6.5.0'

  # Metadata
  spec.summary      = 'Adapter to use YouboraLib on Adswizz SDK'

  spec.description  = <<-DESC
                        YouboraAdswizzAdapter is an adapter used for Adswizz SDK.
                      DESC

  spec.homepage     = 'http://developer.nicepeopleatwork.com/'


  # Spec License
  spec.license      = { :type => 'MIT', :file => 'LICENSE.md' }

  # Author Metadata
  spec.author             = { 'Nice People at Work' => 'support@nicepeopleatwork.com' }

  # Platform
  spec.ios.deployment_target = '11.0'

  spec.swift_version = '5.0', '5.1', '5.2', '5.3'

  # Source Location
  spec.source       = { :git => 'https://bitbucket.org/npaw/adswizz-adapter-ios.git', :tag => spec.version }

  # Source Code
  spec.source_files  = 'YouboraAdswizzAdapter/**/*.{h,m,swift}'
  
  # Project Settings
  spec.pod_target_xcconfig = { 'GCC_PREPROCESSOR_DEFINITIONS' => '$(inherited) YOUBORAADSWIZZADAPTER_VERSION=' + spec.version.to_s }

  # Dependency
  spec.dependency 'YouboraLib', '~> 6.5.0'

end
