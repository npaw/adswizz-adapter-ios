//
//  YBAdswizzAdsAdapter.swift
//  YouboraAdswizzAdapter
//
//  Created by Elisabet Massó on 23/6/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation
import YouboraLib
#if canImport(AdswizzSDK)
import AdswizzSDK

public class YBAdswizzAdsAdapter: YBPlayerAdapter<AnyObject> {
	
    private var delegate: AdManagerDelegate?
    private var viewDelegate: AdVideoViewDelegate?
    private var interactiveDelegate: InteractiveAdManagerDelegate?
        
    private var quartile = 0

    private var adDuration: Double?
    private var adPlayhead: Double?
    private var adSkipped = true
    
    private override init() {
        super.init()
    }
    
    public init(player: AdManager) {
        super.init(player: player)
    }
    
    public override func registerListeners() {
        super.registerListeners()
        if let player = player as? AdManager {
            delegate = player.delegate
            interactiveDelegate = player.interactiveDelegate
            viewDelegate = player.adManagerSettings.videoView?.delegate
            player.delegate = self
            player.interactiveDelegate = self
            player.adManagerSettings.videoView?.delegate = self
        }
        monitorPlayhead(withBuffers: true, seeks: false, andInterval: 1000)
        resetValues()
    }
    
    public override func unregisterListeners() {
        if let player = player as? AdManager {
            player.delegate = delegate
            player.interactiveDelegate = interactiveDelegate
            player.adManagerSettings.videoView?.delegate = viewDelegate
        }
        delegate = nil
        viewDelegate = nil
        interactiveDelegate = nil
        resetValues()
        monitor?.stop()
        super.unregisterListeners()
    }
    
    func resetValues() {
        adDuration = nil
        adPlayhead = nil
        adSkipped = true
        quartile = 0
    }
    
    // MARK: - Getters

    public override func getDuration() -> NSNumber? {
        guard let adDuration = adDuration else { return super.getDuration() }
        return NSNumber(value: adDuration)
    }

    public override func getPlayhead() -> NSNumber? {
        guard let adPlayhead = adPlayhead else { return super.getPlayhead() }
        return NSNumber(value: adPlayhead)
    }
    
    public override func fireStart(_ params: [String : String]?) {
        fireAdBreakStart()
        super.fireStart(params)
    }
    
    public override func fireStop(_ params: [String : String]?) {
        super.fireStop(params)
        resetValues()
    }
    
    // MARK: - Adapter info
    
    public override func getPlayerName() -> String? {
        return Constants.getAdsAdapterName()
    }
    
    public override func getPlayerVersion() -> String? {
        return Constants.getName()
    }
    
    public override func getVersion() -> String {
        return Constants.getAdsAdapterVersion()
    }
    
}

extension YBAdswizzAdsAdapter: AdManagerDelegate {
    
    public func adManager(_ adManager: AdManager, didReceiveAd ad: AdData?, error: Error) {
        delegate?.adManager(adManager, didReceiveAd: ad, error: error)
        let error = error as NSError
        YBSwiftLog.debug("AdManager error: \(error.localizedDescription)")
        fireFatalError(withMessage: error.localizedDescription, code: error.code.description, andMetadata: nil)
    }
    
    public func adManager(_ adManager: AdManager, didReceiveAdEvent event: AdEvent) {
        delegate?.adManager(adManager, didReceiveAdEvent: event)
        
        switch event.event {
            case .unknown,
                 .preparingForPlay:
                break
            case .readyForPlay:
                adDuration = event.ad?.duration
                fireStart()
            case .didStartPlaying:
                adDuration = event.ad?.duration
                fireJoin()
                fireResume()
            case .didPausePlaying:
                firePause()
            case .firstQuartile:
                if quartile == 0 {
                    quartile = 1
                    fireQuartile(1)
                }
            case .midpoint:
                if quartile == 1 {
                    quartile = 2
                    fireQuartile(2)
                }
            case .thirdQuartile:
                if quartile == 2 {
                    quartile = 3
                    fireQuartile(3)
                }
            case .complete:
                adSkipped = false
            case .didFinishPlaying:
                break
            case .adCompleted:
                if !adSkipped {
                    fireStop()
                } else {
                    fireSkip()
                }
                fireAdBreakStop()
            case .allAdsCompleted:
                break
            default:
                break
        }
    }
    
    public func adManager(_ adManager: AdManager, ad: AdData, didChangedPlayHead playHead: TimeInterval, adDuration duration: TimeInterval) {
        delegate?.adManager?(adManager, ad: ad, didChangedPlayHead: playHead, adDuration: duration)
        adDuration = duration
        adPlayhead = playHead
    }
    
}

extension YBAdswizzAdsAdapter: AdVideoViewDelegate {
    
    public func adVideoView(_ adVideoView: AdVideoView, shouldOverrideVideoClickThrough url: URL) -> Bool {
        fireClick(withAdUrl: url.absoluteString)
        return viewDelegate?.adVideoView?(adVideoView, shouldOverrideVideoClickThrough: url) ?? false
    }
    
    public func adVideoViewWillLeaveApplication(_ adVideoView: AdVideoView) {
        viewDelegate?.adVideoViewWillLeaveApplication?(adVideoView)
    }
    
}

extension YBAdswizzAdsAdapter: InteractiveAdManagerDelegate {
    
    public func interactiveAdManager(_ adManager: AdManagerOwner, shouldPresentCoupon pkpassData: Data) {
        interactiveDelegate?.interactiveAdManager(adManager, shouldPresentCoupon: pkpassData)
    }
    
    public func interactiveAdManager(_ adManager: AdManagerOwner, didReceiveAdEvent event: InteractiveAdEvent) {
        interactiveDelegate?.interactiveAdManager(adManager, didReceiveAdEvent: event)
        switch event.event {
            case .skipped:
                // Ad was skipped as a result of an action during interactive ad
                fireSkip()
            default:
                break
        }
    }
    
}
#endif
