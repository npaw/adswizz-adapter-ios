//
//  Constants.swift
//  YouboraAdswizzAdapter
//
//  Created by Elisabet Massó on 23/6/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation

final class Constants {
    
    static func getAdapterName() -> String {
        return "\(getName())-\(getPlatform())"
    }
    
    static func getAdapterVersion() -> String {
        return "\(getVersion())-\(getAdapterName())"
    }
    
    static func getAdsAdapterName() -> String {
        return "\(getName())-Ads-\(getPlatform())"
    }
    
    static func getAdsAdapterVersion() -> String {
        return "\(getVersion())-\(getAdsAdapterName())"
    }
    
    static func getName() -> String {
        return "Adswizz"
    }
    
    static func getPlatform() -> String {
        #if os(tvOS)
        return "tvOS"
        #else
        return "iOS"
        #endif
    }
    
    static func getVersion() -> String {
        guard let path = Bundle(for: self).url(forResource: "Info", withExtension: "plist"), let values = NSDictionary(contentsOf: path), let version = values["CFBundleShortVersionString"] as? String else {
            return ""
        }
        return version
    }
    
}
